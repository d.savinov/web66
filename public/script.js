window.addEventListener('DOMContentLoaded', function (event) {
  
  //константы стоимостей
  const PRICE_1 = 399;
  const PRICE_2 = 10000;
  const PRICE_3 = 999;
  const EXTRA_PRICE = 99;
  let price = PRICE_1;
  let extraPrice = EXTRA_PRICE;
  let s = document.getElementsByName("myselect");

  // Скрываем или показываем радиокнопки.
  s[0].addEventListener("change", function(event) {
    let checks = document.getElementById("mycheck");
    let select = event.target;
    let radios = document.getElementById("myradios"); 
    if (select.value === "1") {
      radios.classList.add("d-none");
      checks.classList.add("d-none");
      extraPrice = EXTRA_PRICE;
      price = PRICE_1;
    }
    if(select.value === "2") {
      radios.classList.remove("d-none");
      checks.classList.add("d-none");
      document.getElementById("radio1").checked = true;
      extraPrice = EXTRA_PRICE;
      price = PRICE_2;
    }
    if(select.value === "3"){
      radios.classList.add("d-none");
      checks.classList.remove("d-none");
      extraPrice = EXTRA_PRICE;
      price = PRICE_3;
      document.getElementById("check").checked = false;
    }
  });

// Смотрим какие товарные свойства выбраны.
let r = document.querySelectorAll(".pis input[type=radio]");
r.forEach(function (currentRadio) {
  const RADIO_1 = 10;
  const RADIO_2 = 15;
  const RADIO_3 = 20;
    currentRadio.addEventListener("change", function (event) {      
        let r = event.target;
        if (r.value === "r1") {      
            extraPrice = RADIO_1;
            console.log(r.value);
        }
        if (r.value === "r2") {
            extraPrice = RADIO_2;
            console.log(r.value);
        }
        if (r.value === "r3") {
            extraPrice = RADIO_3;
            console.log(r.value);
        }
    });
});

//установка доп значения по чекбокусу
const CHECKBOX = -50;
let checkbox = document.getElementById("check");
checkbox.addEventListener("change", function () {
  if (checkbox.checked) {
      extraPrice = CHECKBOX;
  } else {
      extraPrice = EXTRA_PRICE;
  }
});

//калькулятор по количеству
let result = document.getElementById("result");
let count = document.getElementsByName("count");
let calc = document.getElementById("calc");
calc.addEventListener("change", function () {
  let toCount = count[0].value;
  if(toCount.match(/^\d+$/) !== null){
    result.innerHTML=(price + extraPrice) * parseInt(count[0].value);
  }
  else{
      result.innerHTML = "Incorrect input!";
  }
});
});

